def add(a, b):
    '''
    Retrurns the sum of `a` and `b`
    '''
    return a + b

def multiply(a, b):
    '''
    Retrurns the product of `a` and `b`
    '''
    return a * b

def divide(a, b): return a / b
